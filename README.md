# ng poland 2023

## agenda
Alex Rickabaugh / Pawel Kozlowski Exploring Control Flow and Reactivity

[Manfred Steyer - Angular-Architectures with Signals](#manfred-steyer---angular-architectures-with-signals)

Michael Hladky - 3 Reactive Primitives – Angular Architecture With RxJS and Signals

[Rainer Hahnekamp - There’s New Sheriff in Town](#rainer-hahnekamp---theres-new-sheriff-in-town)

[Younes Jaaidi - Fake it till you Mock it](#younes-jaaidi---fake-it-till-you-mock-it)

Alex Okrushko - Angular Signals are here. Is NgRx now obsolete?

[Tomasz Ducin - Thinking in Signals](#tomasz-ducin---thinking-in-signals)

[Matt Lewis - Making Development Times Fast With Esbuild](#matt-lewis---making-development-times-fast-with-esbuild)

[Miško Hevery - Comparative Study of Reactivity Across Frameworks and Implications for Resumability](#miško-hevery---comparative-study-of-reactivity-across-frameworks-and-implications-for-resumability)

[Julian Jandl - Virtual Scrolling Unveiled: High Performance Lists in Angular](#julian-jandl---virtual-scrolling-unveiled-high-performance-lists-in-angular)

Mateusz Łędzewicz - To Module or Not to Module, That Is the Question!

~~Alisa Duncan - Introducing the Identity Guardians~~

[Michael Egger-Zikes - The Internal Magic of Angular Signals](#michael-egger-zikes---the-internal-magic-of-angular-signals)

[Christopher Holder - Managing Design Systems With Storybook and Angular](#christopher-holder---managing-design-systems-with-storybook-and-angular)

## Manfred Steyer - Angular-Architectures with Signals
[angulararchitects.io](https://www.angulararchitects.io/)

[Prezentacja](./slides/signals-arc.pdf)

## Rainer Hahnekamp - There’s New Sheriff in Town
[Prezentacja - film](https://www.youtube.com/watch?v=yxVI6sAo8fU)

Biblioteka do pilnowania zależności w projektach bez NX, ale działającą na podobnej zasadzie :)
Wystąpienie z innej konferencji ale w podobnym tonie:

## Younes Jaaidi - Fake it till you Mock it
[GitHub](https://github.com/yjaaidi)

[Prezentacja](./slides/fake-it-till-you-mock-it.pdf)

Prezentacja na temat testowania: stub / mock / spy / fake i dlaczego powinniśmy bardziej przyjrzeć się podejściu fake.

## Tomasz Ducin - Thinking in Signals

[Prezentacja](https://slides.com/ducin/thinking-in-signals)

## Matt Lewis - Making Development Times Fast With Esbuild

Zajawka ESBuild. Wielki projekt jakim jest [ClickUp](https://clickup.com) zmaga się z czasem buildów developerskich.
Tutaj opis całkiem spoko streszczenia tego co było na [prezentacji](https://blog.stackademic.com/mythical-angular-you-all-need-to-know-about-this-e1fe628e0259)

## Miško Hevery - Comparative Study of Reactivity Across Frameworks and Implications for Resumability

[Prezentacja-film](https://www.youtube.com/watch?v=vf1v7n2ApHI)

Nie z ng poland ale prezentacja ta sama ;)

## Julian Jandl - Virtual Scrolling Unveiled: High Performance Lists in Angular

[Prezentacja - film](https://www.youtube.com/watch?v=tUAwhEHF74g)
Całkiem fajna prezentacja na temat virtual scrolla i viewportów ;)


## Christopher Holder - Managing Design Systems With Storybook and Angular

[Prezentacja - film](https://www.youtube.com/watch?v=VmgOlTffwag)

## Michael Egger-Zikes - The Internal Magic of Angular Signals

[Prezentacja](https://speakerdeck.com/michaeleggerzikes/2023-66f34b32-4ac9-4298-81b9-d8b1585591ad)
